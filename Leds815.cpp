#include "Leds815.h"
#include <X11/Xlib.h>

ResourceManager *Leds815::_RM = nullptr;

Leds815::Leds815() {
	_allowColorChange = false;
	auto display_addr = getenv( "DISPLAY" );
	if ( !display_addr || !*display_addr ) return;

	_display = XOpenDisplay( display_addr );
	_thread	 = std::jthread( [this]( std::stop_token stoken ) { loop( stoken ); } );
}

Leds815::~Leds815() {
	_thread.request_stop();
	_thread.join();
	if ( _display ) {
		XCloseDisplay( (Display *)_display );
		_display = nullptr;
	}
}

OpenRGBPluginInfo Leds815::Initialize( bool, ResourceManager *RM ) {
	info.PluginName		   = "Leds815";
	info.PluginDescription = "Change color for ledded keys (caps, scrlk, np). X11 only";
	info.PluginLocation	   = "TopTabBar";
	info.HasCustom		   = true;
	info.PluginLabel	   = new QLabel( "Leds815" );

	_RM = RM;

	return info;
}

QWidget *Leds815::CreateGUI( QWidget *Parent ) {
	_RM->WaitForDeviceDetection();
	_allowColorChange = true;

	_ui = new Leds815Tab( Parent );

	QObject::connect( this, &Leds815::onCapsToggled, _ui, &Leds815Tab::CapsLockToggled );
	QObject::connect( this, &Leds815::onNumToggled, _ui, &Leds815Tab::NumLockToggled );
	QObject::connect( this, &Leds815::onScrollToggled, _ui, &Leds815Tab::ScrollLockToggled );
	QObject::connect( this, &Leds815::onUpdate, _ui, &Leds815Tab::UpdateLeds );
	QObject::connect( this, &Leds815::onDevClear, _ui, &Leds815Tab::Clear );
	QObject::connect( this, &Leds815::onDevReady, _ui, &Leds815Tab::LoadDevices );

	_prevCaps	= !isCapsLockEnabled();
	_prevNum	= !isNumLockEnabled();
	_prevScroll = !isScrollLockEnabled();

	_RM->RegisterDetectionStartCallback( &Leds815::DetectionStart, this );
	_RM->RegisterDetectionEndCallback( &Leds815::DetectionEnd, this );

	return _ui;
}

bool Leds815::isCapsLockEnabled() const {
	if ( !_display ) return false;
	XKeyboardState state;
	XGetKeyboardControl( (Display *)_display, &state );
	return state.led_mask & (ulong)LedMasks::kCapsLock;
}

bool Leds815::isNumLockEnabled() const {
	if ( !_display ) return false;
	XKeyboardState state;
	XGetKeyboardControl( (Display *)_display, &state );
	return state.led_mask & (ulong)LedMasks::kNumLock;
}

bool Leds815::isScrollLockEnabled() const {
	if ( !_display ) return false;
	XKeyboardState state;
	XGetKeyboardControl( (Display *)_display, &state );
	return state.led_mask & (ulong)LedMasks::kScrollLock;
}

void Leds815::loop( std::stop_token stoken ) {
	while ( !stoken.stop_requested() ) {
		std::this_thread::sleep_for( kTimeout );
		if ( !_allowColorChange ) continue;

		bool anyChanged = false;
		bool caps		= isCapsLockEnabled();
		bool num		= isNumLockEnabled();
		bool scroll		= isScrollLockEnabled();

		if ( caps != _prevCaps ) {
			anyChanged = true;
			_prevCaps  = caps;
			emit onCapsToggled( caps );
		}
		if ( num != _prevNum ) {
			anyChanged = true;
			_prevNum   = num;
			emit onNumToggled( num );
		}
		if ( scroll != _prevScroll ) {
			anyChanged	= true;
			_prevScroll = scroll;
			emit onScrollToggled( scroll );
		}
		if ( !anyChanged ) emit onUpdate();
	}
}

void Leds815::devClear() {
	emit onDevClear();
}

void Leds815::devReady() {
	emit onDevReady();
}

void Leds815::DetectionStart( void *_this ) {
	auto self = (Leds815 *)_this;

	self->_allowColorChange = false;
	QMetaObject::invokeMethod( self, "devClear", Qt::QueuedConnection );
}

void Leds815::DetectionEnd( void *_this ) {
	auto self = (Leds815 *)_this;

	self->_allowColorChange = true;
	QMetaObject::invokeMethod( self, "devReady", Qt::QueuedConnection );
}
