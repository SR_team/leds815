#include "LedColor.h"

LedColor::LedColor( QWidget *parent ) : QWidget( parent ) {
	setupUi( this );
}

void LedColor::setColor( ColorType type, const QColor &color ) {
	switch ( type ) {
		case ColorType::kDisabled:
			clDisabled->setColor( color );
			break;
		case ColorType::kEnabled:
			clEnabled->setColor( color );
			break;
	}
}

QColor LedColor::color( ColorType type ) const {
	switch ( type ) {
		case ColorType::kDisabled:
			return clDisabled->color();
		case ColorType::kEnabled:
			return clEnabled->color();
		default:
			return QColor();
	}
}

void LedColor::setEnabledLED( bool enabled ) {
	ledEnabled->setChecked( enabled );
}

bool LedColor::isEnabledLED() const {
	return ledEnabled->isChecked();
}

void LedColor::changeEvent( QEvent *e ) {
	QWidget::changeEvent( e );
	switch ( e->type() ) {
		case QEvent::LanguageChange:
			retranslateUi( this );
			break;
		default:
			break;
	}
}
