#ifndef LEDS815_GLOBAL_H
#define LEDS815_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(LEDS815_LIBRARY)
#  define LEDS815_EXPORT Q_DECL_EXPORT
#else
#  define LEDS815_EXPORT Q_DECL_IMPORT
#endif

#endif // LEDS815_GLOBAL_H
