#ifndef LEDS815_H
#define LEDS815_H

#include "Leds815_global.h"
#include <thread>
#include <atomic>
#include <chrono>
#include <QObject>
#include <OpenRGBPluginInterface.h>
#include "Leds815Tab.h"

class LEDS815_EXPORT Leds815 : public QObject, public OpenRGBPluginInterface {
	Q_OBJECT
	Q_PLUGIN_METADATA( IID OpenRGBPluginInterface_IID )
	Q_INTERFACES( OpenRGBPluginInterface )

	enum class LedMasks : unsigned long { kCapsLock = 1, kNumLock = 2, kScrollLock = 4 };

	constexpr static std::chrono::milliseconds kTimeout{ 50 };

public:
	Leds815();
	~Leds815();

	OpenRGBPluginInfo Initialize( bool, ResourceManager *RM ) override;
	QWidget *		  CreateGUI( QWidget *Parent ) override;

	static ResourceManager *_RM;
	Leds815Tab *			_ui = nullptr;

	bool isCapsLockEnabled() const;
	bool isNumLockEnabled() const;
	bool isScrollLockEnabled() const;

protected:
	void *			  _display = nullptr;
	std::atomic<bool> _allowColorChange;
	std::jthread	  _thread;

	bool _prevCaps	 = false;
	bool _prevNum	 = false;
	bool _prevScroll = false;

	void loop( std::stop_token stoken );

public slots:
	void devClear();
	void devReady();

signals:
	void onCapsToggled( bool enabled );
	void onNumToggled( bool enabled );
	void onScrollToggled( bool enabled );
	void onUpdate();
	void onDevClear();
	void onDevReady();

private:
	static void DetectionStart( void *_this );
	static void DetectionEnd( void *_this );
};

#endif // LEDS815_H
