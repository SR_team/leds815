#include "Leds815Tab.h"
#include "Leds815.h"

Leds815Tab::Leds815Tab( QWidget *parent ) : QWidget( parent ) {
	setupUi( this );
	LoadDevices();
	LoadLEDs();
}

void Leds815Tab::CapsLockToggled( bool enabled ) {
	if ( !capslock->isEnabledLED() ) return;

	for ( int i = 0; i < kbdList->count(); ++i ) {
		auto kbd = kbdList->item( i );
		if ( kbd->checkState() != Qt::Checked ) continue;

		auto ctrl = getController( i );
		if ( !ctrl ) continue;

		auto color = capslock->color( enabled ? LedColor::ColorType::kEnabled : LedColor::ColorType::kDisabled );
		ApplyColor( ctrl, "Caps Lock", color );
	}
}

void Leds815Tab::NumLockToggled( bool enabled ) {
	if ( !numlock->isEnabledLED() ) return;

	for ( int i = 0; i < kbdList->count(); ++i ) {
		auto kbd = kbdList->item( i );
		if ( kbd->checkState() != Qt::Checked ) continue;

		auto ctrl = getController( i );
		if ( !ctrl ) continue;

		auto color = numlock->color( enabled ? LedColor::ColorType::kEnabled : LedColor::ColorType::kDisabled );
		ApplyColor( ctrl, "Num Lock", color );
	}
}

void Leds815Tab::ScrollLockToggled( bool enabled ) {
	if ( !scrolllock->isEnabledLED() ) return;

	for ( int i = 0; i < kbdList->count(); ++i ) {
		auto kbd = kbdList->item( i );
		if ( kbd->checkState() != Qt::Checked ) continue;

		auto ctrl = getController( i );
		if ( !ctrl ) continue;

		auto color = scrolllock->color( enabled ? LedColor::ColorType::kEnabled : LedColor::ColorType::kDisabled );
		ApplyColor( ctrl, "Scroll Lock", color );
	}
}

void Leds815Tab::UpdateLeds() {
	// may be skip validation?
}

void Leds815Tab::ApplyColor( RGBController *ctrl, std::string_view name, const QColor &color ) {
	for ( int i = -1; auto &&led : ctrl->leds ) {
		++i;

		if ( led.name.find( name ) == std::string::npos ) continue;

		auto rgbColor = ToRGBColor( color.red(), color.green(), color.blue() );
		if ( ctrl->GetLED( i ) == rgbColor ) continue;
		ctrl->SetLED( i, rgbColor );
		ctrl->UpdateLEDs();
	}
}

void Leds815Tab::Clear() {
	SaveSettings();
	kbdList->clear();
}

void Leds815Tab::LoadDevices() {
	auto ctrls = Leds815::_RM->GetRGBControllers();
	auto sets  = Leds815::_RM->GetSettingsManager();

	for ( auto &&ctrl : ctrls ) {
		if ( ctrl->type != DEVICE_TYPE_KEYBOARD ) continue;
		if ( ctrl->leds.size() <= 1 ) continue;

		kbdList->addItem( ctrl->name.data() );
		auto kbd = kbdList->item( kbdList->count() - 1 );
		kbd->setData( Qt::UserRole, ctrl->serial.data() );

		try {
			auto json = sets->GetSettings( "Leds815" );
			auto set  = json.value<bool>( ctrl->name + "/" + ctrl->serial, false );
			kbd->setCheckState( set ? Qt::Checked : Qt::Unchecked );
		} catch ( json::exception &e ) {
			kbd->setCheckState( Qt::Unchecked );
		}
	}
}

void Leds815Tab::LoadLEDs() {
	auto sets = Leds815::_RM->GetSettingsManager();

	auto readColor = []( const json &json ) -> QColor {
		auto r = json.value<int>( "red", 255 );
		auto g = json.value<int>( "green", 255 );
		auto b = json.value<int>( "blue", 255 );
		return QColor( r, g, b );
	};

	try {
		auto json	= sets->GetSettings( "Leds815" );
		auto caps	= json.value( "CapsLock", json::object() );
		auto num	= json.value( "NumLock", json::object() );
		auto scroll = json.value( "ScrollLock", json::object() );

		capslock->setColor( LedColor::ColorType::kDisabled, readColor( caps.value( "disabled", json::object() ) ) );
		capslock->setColor( LedColor::ColorType::kEnabled, readColor( caps.value( "enabled", json::object() ) ) );
		capslock->setEnabledLED( caps.value<bool>( "use", true ) );

		numlock->setColor( LedColor::ColorType::kDisabled, readColor( num.value( "disabled", json::object() ) ) );
		numlock->setColor( LedColor::ColorType::kEnabled, readColor( num.value( "enabled", json::object() ) ) );
		numlock->setEnabledLED( caps.value<bool>( "use", true ) );

		scrolllock->setColor( LedColor::ColorType::kDisabled, readColor( scroll.value( "disabled", json::object() ) ) );
		scrolllock->setColor( LedColor::ColorType::kEnabled, readColor( scroll.value( "enabled", json::object() ) ) );
		scrolllock->setEnabledLED( caps.value<bool>( "use", true ) );
	} catch ( json::exception &e ) {
	}
}

void Leds815Tab::SaveSettings() {
	auto sets = Leds815::_RM->GetSettingsManager();

	auto writeColor = []( const QColor &color ) -> json {
		auto json	  = json::object();
		json["red"]	  = color.red();
		json["green"] = color.green();
		json["blue"]  = color.blue();
		return json;
	};

	try {
		auto json = json::object();

		for ( int i = 0; i < kbdList->count(); ++i ) {
			auto kbd   = kbdList->item( i );
			auto name  = kbd->text().toStdString() + "/" + kbd->data( Qt::UserRole ).toString().toStdString();
			json[name] = kbd->checkState() == Qt::Checked;
		}

		auto color = json::object();

		color["disabled"] = writeColor( capslock->color( LedColor::ColorType::kDisabled ) );
		color["enabled"]  = writeColor( capslock->color( LedColor::ColorType::kEnabled ) );
		color["use"]	  = capslock->isEnabledLED();
		json["CapsLock"]  = color;

		color["disabled"] = writeColor( numlock->color( LedColor::ColorType::kDisabled ) );
		color["enabled"]  = writeColor( numlock->color( LedColor::ColorType::kEnabled ) );
		color["use"]	  = numlock->isEnabledLED();
		json["NumLock"]	  = color;

		color["disabled"]  = writeColor( scrolllock->color( LedColor::ColorType::kDisabled ) );
		color["enabled"]   = writeColor( scrolllock->color( LedColor::ColorType::kEnabled ) );
		color["use"]	   = scrolllock->isEnabledLED();
		json["ScrollLock"] = color;

		sets->SetSettings( "Leds815", json );
	} catch ( json::exception &e ) {
	}
}

void Leds815Tab::changeEvent( QEvent *e ) {
	QWidget::changeEvent( e );
	switch ( e->type() ) {
		case QEvent::LanguageChange:
			retranslateUi( this );
			break;
		default:
			break;
	}
}

void Leds815Tab::hideEvent( QHideEvent * ) {
	SaveSettings();
}

RGBController *Leds815Tab::getController( int row ) {
	if ( row < 0 || row > kbdList->count() ) return nullptr;

	auto &ctrls = Leds815::_RM->GetRGBControllers();

	for ( auto &&ctrl : ctrls ) {
		if ( ctrl->type != DEVICE_TYPE_KEYBOARD ) continue;
		if ( kbdList->item( row )->text() != ctrl->name.data() ) continue;
		if ( kbdList->item( row )->data( Qt::UserRole ).toString() != ctrl->serial.data() ) continue;

		return ctrl;
	}

	return nullptr;
}
