#ifndef LEDS815TAB_H
#define LEDS815TAB_H

#include "ui_Leds815Tab.h"

class Leds815Tab : public QWidget, private Ui::Leds815Tab {
	Q_OBJECT

public:
	explicit Leds815Tab( QWidget *parent = nullptr );

public slots:
	void CapsLockToggled( bool enabled );
	void NumLockToggled( bool enabled );
	void ScrollLockToggled( bool enabled );
	void UpdateLeds();
	void ApplyColor( class RGBController *ctrl, std::string_view name, const QColor &color );
	void Clear();
	void LoadDevices();
	void LoadLEDs();
	void SaveSettings();

protected:
	void changeEvent( QEvent *e ) override;
	void hideEvent( QHideEvent * ) override;

	class RGBController *getController( int row );
};

#endif // LEDS815TAB_H
